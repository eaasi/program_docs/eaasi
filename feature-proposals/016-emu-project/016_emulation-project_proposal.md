**Emulation Project - Feature Set Proposal**
-

# Summary

The emulation project interface provides a central location for the compilation of resources and initial configuration of an environment session. Users may add resources to their emulation project while exploring the contents of the node. Once they've compiled the necessary resources, they select which ones they would like to use in their environment session (e.g., a base environment and an object). With resources selected and UI settings configured, the user will "Run" their environment to start a configuration session.

The Emulation Project workflow also supports creation of "root" environments that are not derived from existing images in the node. The system provides templates of hardware configurations designed for specific computing environments (e.g., Windows 98 PC, Apple System 8 PPC) from which the user will select the most appropriate for their new environment. Users may install an operating system using objects from the library or assign an existing disk image (e.g., content environment) to the boot disk.

# Problem To Solve

- Stakeholders require uniform mechanisms and workflows for interaction with emulation components
- The Emulation Project would provide a summary view of environment components prior to launch. This ensures users consider the combination of components and provides an opportunity to make suggestions via UVI.

# Impacted Editions

Hosted and Local

# Analysis

## Business Rules/Requirements
- An emulation environment should have an associated hardware template (whether selected or inherited from the base environment)
- An emulation environment may have only one software object or content object selected for environment config
- An emulation project may have any number of resources in its queue/cart

## Use Cases
- [Create Content Environment from Disk Image](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/016-emu-project/use-cases/create_content_env.md)
- [Create content environment with content object](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/016-emu-project/use-cases/create_content_object_environment.md)
- [Create Derivative Base Environment](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/016-emu-project/use-cases/create_derivative_base_environment.md)
- [Create New Base Environment](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/016-emu-project/use-cases/create_new_base_environment.md)
- [Revise Content Environment](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/016-emu-project/use-cases/revise_content_environment.md)
- Create computing network

## Risks

| Type | Description |
| ------ | ------ |
| System Design | Users don't understand best practices for setting up their emulation environment |
| User Behavior | Users select an object that isn't compatible with the selected base environment |
| User Behavior | Users use the emulation project in place of the bookmarking feature |
| User Behavior | Users compile too many resources in the project and are unable to easily find them in their queue |

## Designs
https://xd.adobe.com/view/47926ad4-0c55-4983-7d83-6cbcc5ad6ad3-31a3/

## Proposed Iterations

**1. [Basic emulation project interface](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/790)**
- Users add environment or object to queue
- Select base environment and object to work with
- Configure environment settings (can print, relative mouse, clean shutdown, etc.)
- Advanced settings (virtualization, webrtc, etc.)

**2. Expanded metadata, component presentation, and addition of computing network configuration**
- Components presented separately: Machine template, software environment contents, object details
- Addition of computer network configuration

**3. Advanced configuration options**
- Based on emulator capabilities: allow disc configuration (e.g., load CD object in CD drive)
- Customized hardware configurations
  - Save machine templates based on changes

**4. Incorporate UVI recommendations**

# Dev Team Response

# Open Questions
- How does network configuration fit into the emulation project interface?
