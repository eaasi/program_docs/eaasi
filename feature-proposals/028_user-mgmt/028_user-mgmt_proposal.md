# Problem To Solve

Nodes need to control the actions and resources available to a user based on the individual context of each user, including the user's organization/node, role, permissions, etc. Without suitable controls, EaaSI cannot provide sufficient security to enable use of the system and access to resources at scale. To ensure a secure system, a user's context must be enforced in all workflows and layers of the system.

This proposal together with !17 address configuration and enforcement of user context.

# Associated Editions

[x] Hosted
[x] Local

# Analysis

### Business Rules/Functional Requirements

- The system must provide user roles with associated authorizations and restrictions for actions, workflows, and pages in the system
- The system must enforce permissions according to a resource's assigned access level (e.g., node, network)
- The system must create, edit, and delete user account information as specified by admins
- The system must enforce permitted user actions based on their assigned role
- System must authenticate user from credentials (email/username and password) supplied upon login
- System must securely store authentication credentials against common threats
- System must provide an option to recover or reset a password if a user is unable to login with existing info

### Use Cases

- [Admin creates new account](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/028_user-mgmt/028_use-cases/028_account-creation.md)
- [Admin deletes account](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/028_user-mgmt/028_use-cases/028_delete-account.md)
- [User login with EaaSI credentials](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/028_user-mgmt/028_use-cases/028_login.md)
- [Password reset](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/028_user-mgmt/028_use-cases/028_password-reset.md)
- [End user accesses environment via shared link](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/028_user-mgmt/028_use-cases/028_access-user-context.md)
- [User attempts to publish to network](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/028_user-mgmt/028_use-cases/028_role-authorization.md)

### Risks
| Type | Description |
| ------ | ------ |
| User Behavior | Admin assigns incorrect role and access user is able to view/alter environments |
| System Design | User roles are too restrictive or lenient for node use cases |
| Security | Breach of credentials stored and managed by EaaSI |

# Proposed Iterations

**1. Simple account creation/authentication**
- As implemented by PortalMedia, local DB hold user info and authenticates against credentials held there

**2. Keycloak implementation**
- [Group and user account information recorded and managed by Keycloak infrastructure](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/829)
- [User context expanded and enforced across all system components](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/812)
- Access user context
  - [Access user authentication](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/830)
  - [Access user interface](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/836)
  - [Access denied page](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/794)
- [Hosted edition super admin site to setup node(user groups)](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/842)
- [Implementation of new user roles](https://gitlab.com/eaasi/eaasi-client-dev/-/issues/537)
- [Password reset from login page](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/850)

**3. IDP brokering**
- [Local integration with SAML IDP](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/851)

# Dev Approach

- UX/UI Design Updates for User Mgmt #537
- Simple user / group management with keycloak backend https://gitlab.com/openslx/eaas-server/-/issues/79
  - implement keycloak REST facade
  - resolve uuid (user / group) to meaningful strings
  - integrate with S3 resource management
- Extend user context to incorporate groups https://gitlab.com/openslx/eaas-server/-/issues/89
  - username
  - UID
  - GUID
  - role(s)

# Answered Questions
- **do we have a common user base for all institutions? or do we setup subdomains for each institution?** No preference, whatever is simplest and easiest to maintain
- **how are groups (institution affiliations) managed** User account must first be created in the application which would affiliate the user info with the node institution upon creation.
- **are there "sub-admin" roles to manage users?** A node must have at least one admin (sub-admin in the case of the hosted service I suppose) who can create new user accounts
- **Which authentication system shall we use?** Again, whatever is easiest or most convenient for other system components.
