# Content object import

| Field | Description |
| ----- | ----------- |
| Name* | Name given to the digital object |
| Local Identifier | Alternative identifier used by other local systems at node organization |
| Local Identifier Source | Local system or scheme from which identifier is sourced |
| File Name* | Name of file selected for submission (autofill from system when selected by user) |
| Order | Order of use relative to other files associated with manifestation (if relevant) |
| Media Type* | Format of original item (if disk image) or "file" if not a disk image |
| Label | Name or description of file provided by user or from original media |

# Software object import

| Field | Description |
| ----- | ----------- |
| Name* | Name given to digital object |
| Local Identifier | Alternative identifier used by other local systems at node organization |
| Local Identifier Source | Local system or scheme from which identifier is sourced |
| Product Key | String of numbers and letters needed to unlock software for installation |
| File Name* | Name of file selected for submission (autofill from system when selected by user) |
| Order | Order of use relative to other files associated with manifestation (if relevant) |
| Media Type* | Format of original item (if disk image) or "file" if not a disk image |
| Label | Name or description of file provided by user or from original media |

#### Software Included

**May be too much to ask for at this point in the workflow**

| Field | Description |
| ----- | ----------- |
| Title | The name of the software version |
| Version Number | Numeric or symbolic identifier(s) of versions of a software or file format, current and past |
| Is Version Of* | Software product that described item is a version of |
| Alternate Identifier | Alternative identifier from outside source |
| Identifier Source | A description of the source system, org, or other associated manager of the identifier |

# Environment image import

| Field | Description |
| ----- | ----------- |
| Name* | Name given to environment image |
| Description* | Short description of the environment's purpose or contents |
| Local Identifier | Alternative identifier used by other local systems at node organization |
| Local Identifier Source | Local system or scheme from which identifier is sourced |
| Operating System | Operating system installed and configured in environment |
