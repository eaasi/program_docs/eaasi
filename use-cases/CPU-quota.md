# Use Case

**Title** Hosted node hits CPU hours quota

**Pre-condition** Private CPU hours quota set during deployment; User logged into system

**Potential Flow – Preemptive Warning**
1. Upon logging in, the System displays a warning informing the user that they have limited remaining CPU hours for running emulation hours
2. User closes warning window
3. System displays alert text in interface header?

  **Post-condition** Users unable to run emulation environments until quota resets the following month

# Comments
* As discussed during checkin 10/12, enforcing CPU hours is complicated and its still unclear what the best framework for enforcement may be. At this time, we may want to wait and see how many CPU hours are actually used during the pilot program to get a better sense of reasonable limits and how we might enforce them.
