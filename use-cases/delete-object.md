# Use Case

**Title** User deletes object

**Description** User locates object and deletes it. The system removes files and metadata related to the deleted object.

**Pre-condition** Object imported into system; Private object only; Admin user or config user who imported object

**Regular flow**

1. User locates object in Explore Resources or My Resources interface
2. User selects resource card OR opens detail page and clicks Actions
3. System determines which actions are authorized and displays Action Menu
4. User selects Delete
5. System displays warning regarding results of delete action
6. User selects Delete option
7. System deletes files and metadata of object

   **Post-condition** Object no longer available for use in node
