# Use Case

**Title** User resets password

**Description** User requests to reset password, receives temporary password reset link or receives auto-generated password, and updates info for login.

**Pre-Condition** User account created

**Potential Flow 1 - Password reset**
1. User enters clicks Reset Password (or Forgot Password)
2. System notifies user that temporary reset email has been sent
3. User clicks reset link in email
4. System prompts user to create new password
5. User enters and confirms new password
6. Password info is updated in identity management system

   **Post-Condition** User is redirected to Log In page

**Potential Flow 2 - Random password sent**
1. User enters clicks Reset Password (or Forgot Password)
2. System notifies user that email with new password has been sent
3. User clicks login link in email
4. User enters email and new password
5. System prompts user to create new password
6. User enters and confirms new password
7. Password info is updated in identity management system

   **Post-Condition** User is redirected to Log In page

**Regular Flow – Password update after logging in**
1. User clicks icon at top of interface and selects Change Password
2. System displays change password interface
3. User enters current password and new password, twice
4. System confirms new password matches
5. User clicks Save

   **Post-Condition** New password recorded for user authentication

   **Optional Flow – New password doesn't match**

   5. System prompts user to correct password
   6. User re-enters new password and password confirmation
   7. Process resumes at Step 4 above


   **Post-Condition** User redirected to EaaSI Dashboard
