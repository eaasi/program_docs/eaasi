# Use Case

**Title** Save base environment to node from network

**Description** User selects environment and copies it to local node storage, making it available for use in emulation projects.

**Pre-Condition** Environment shared to network; Node synced with most recent data; Local edition only

**Regular Flow**

1. User enters keyword search or browses node contents
2. User locates environment in discovery interface
3. a) User selects resource card OR b) User opens detail page and clicks Actions
4. System displays Action menu [Permitted user actions]
5. User clicks Save to node
6. System notifies user that copying image will use storage space within their node
7. User clicks Save Environment
8. System locates and copies environment data to local storage

   **Post-condition** Environment available for use in an emulation project
