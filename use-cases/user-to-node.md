**Use Case - User to node sharing**
-

**Description:** The creator/importer of an environment or object makes the resource available to all system users within their node.

**Pre-Condition:** User has created or imported the resource in their node or been given access by the original user.

**Ordinary Sequence:**
1. The user opens the resource’s action menu
2. The system displays the available action options, including “Share to Node”

   **Business Rule:** A resource can only be shared to a node by those who created or imported them

3. The user selects “Share to Node”
4. The system prompts the user to confirm this action
5. The user confirms the action to the system
6. The system updates access permissions data for the resource

**Post-Condition:** Resource is available to node users in the Explore Resources interface
