### User Story

_Describe the basic function and outcome of the feature in standard user story form: As a [type of user], I need [proposed function], so that [desired outcome]_

### Edition

_Mark which edition of the system should include the new feature below_

- [ ] Local
- [ ] Hosted
- [ ] Both


### Requirements

_List any known requirements of the proposed feature_.


### Use Cases

_Attach files of any existing use cases describing proposed workflows or link out to markdown documents in the [use-cases directory](https://gitlab.com/eaasi/program_docs/eaasi/-/tree/master/use-cases)_.

### Proposed Iterations

_If relevant, list possible iterations of the feature that could be developed for subsequent releases_.

/label ~"feature"

/cc @sethand
