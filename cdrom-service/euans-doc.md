# CD-ROM Emulation Access (CD-RE Access) Service

**A service to re-enable access to CD-ROM publications in library general collections.**


**Outcome**
-

Libraries get access to skinnable landing pages for each of the CD-ROM based publications in their collections that re-enable access to them using emulation and software preservation. 

**Overview**
-

How the service will work:

1.	The service host will publish a list of CD-ROMs including names, worldcat references (where available), MARC records (where available), Wikidata QIDs and Image checksum values. This list will enable orgs to decide if they want to subscribe. 
2.	Libraries that subscribe to the service will be able to have their libraries’ network’s IP ranges whitelisted for access to the service.
3.	Libraries are required to monitor which CDs they have legal copies of and assume liability for providing end-user access from a copyright perspective
4.	Libraries have a management dashboard which allows them to:
    * Skin their landing pages (change color, logo, intro, terms)
    * Select which CD-ROMs they have legal access to and how many concurrent instances they can legally give access to (automatic access is available to all non/out-of copyright resources) by checking boxes
    * Bulk download URLs to their landing pages, thumbnails, and metadata records for the items they have access to
    * Upload new ISO images and metadata to be configured by the service host
5.	Libraries will add links to the environment landing pages to their catalogues
6.	Issues go through a tracking system to the service host and get triaged from there
7.	Service host configures any new CD-ROMs that are uploaded by libraries and ensures adequate metadata is available
8.	Service Admin requires ability to:
    * Add new CD-ROMs
    * Add new libraries (and their user-reps) to service
    * Remove CD-ROMs
    * Remove orgs
    * Add metadata
    * Add thumbnails
    * Edit existing items
    * Change/fix CD-Environments without changing landing page URLs



